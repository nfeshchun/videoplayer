//
//  ViewController.swift
//  VideoPlayer
//
//  Created by Nikita on 17.10.18.
//  Copyright © 2018 Nikita Feshchun. All rights reserved.
//

import UIKit
import TwitterReader

class ViewController: UIViewController, VLCMediaPlayerDelegate {
    
    @IBOutlet weak var preloader: UIActivityIndicatorView!
    @IBOutlet weak var movieView: UIView!
    
    let interactor = TwitterInteractor()
    var mediaPlayer: VLCMediaPlayer = VLCMediaPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preparePlayer()
    }
    
    // MARK: Actions
    
    @IBAction func urlAction(_ sender: Any) {
        showAlert()
    }
    
    @IBAction func twitterAction(_ sender: Any) {
    
        interactor.atachTL(for: self.view)
        interactor.prepareSource(for: "feshchun")
    }
    
    @IBAction func playAction(_ sender: Any)
    {
        mediaPlayer.play()
    }
    
    @IBAction func pauseAction(_ sender: Any)
    {
         mediaPlayer.pause()
    }
    
    // MARK: VLCMediaPlayerDelegate
    
    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        
        switch mediaPlayer.state {
        case .playing:
            stopPreloader()
            break
        case .error:
            stopPreloader()
            AlertProvider.errorAlert(text: "Invalid URL", presenter: self)
            break
        case .stopped:
            stopPreloader()
            break
        default:
            break
        }
    }
    
    // MARK: BL
    
    private func stopPreloader()
    {
        preloader.isHidden = true
        preloader.stopAnimating()
    }
    
    private func startPreloader()
    {
        preloader.isHidden = false
        preloader.startAnimating()
    }
    
    private func preparePlayer()
    {
        mediaPlayer.delegate = self
        mediaPlayer.drawable = self.movieView
        playDefaultVideo()
    }
    
    private func playDefaultVideo()
    {
        guard let url = URL(string: "https://media.w3.org/2018/02/webrtc-20180221.mp4") else {
            AlertProvider.errorAlert(text: "Invalid URL", presenter: self)
            return
        }
        self.setupMedia(url: url)
    }
    
    
    private func showAlert()
    {
        AlertProvider.urlInputAlert(presenter: self) { [unowned self] (urlText) in
            
            guard let url = URL(string: urlText) else {
                AlertProvider.errorAlert(text: "Invalid URL", presenter: self)
                return
            }
            self.setupMedia(url: url)
        }
    }
    
    private func setupMedia(url: URL)
    {
        startPreloader()
        let media = VLCMedia(url: url)
        self.mediaPlayer.media = media
        self.mediaPlayer.play()
    }
}
