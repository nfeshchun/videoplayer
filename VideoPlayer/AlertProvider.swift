//
//  AlertInteractor.swift
//  VideoPlayer
//
//  Created by Nikita on 18.10.18.
//  Copyright © 2018 Nikita Feshchun. All rights reserved.
//

import Foundation

class AlertProvider
{
    class func errorAlert(text: String, presenter: UIViewController)
    {
        let alertController = UIAlertController(title: "Error", message:
            text, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
        
        presenter.present(alertController, animated: true, completion: nil)
    }
    
    class func urlInputAlert(presenter: UIViewController, readTextAction: @escaping (String) -> Void)
    {
        let alertController = UIAlertController(title: "Enter Url", message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = "Url"
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default) { (UIAlertAction) in
            alertController.dismiss(animated: true, completion: nil)
        })
        
        alertController.addAction(UIAlertAction(title: "Done", style: .default, handler: { (UIAlertAction) in
          
            guard let urlText = alertController.textFields?.first?.text else {
                AlertProvider.errorAlert(text: "Error entering URL", presenter: presenter)
                return
            }
            
            readTextAction(urlText)
            
            alertController.dismiss(animated: true, completion: nil)
        }))
        
        presenter.present(alertController, animated: true, completion: nil)
    }

}

